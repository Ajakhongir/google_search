from selenium import webdriver
import csv
import time

print("Please enter the Keyword")
keyword = str(input())
time.sleep(2)
print("Great")
print("Please now enter the amount of page you want to get data from!")
pagination = int(input())
time.sleep(3)

url = f"https://www.google.co.za/search?q={keyword}"

driver = webdriver.Chrome("chromedriver.exe")
driver.maximize_window()

driver.get(url)
time.sleep(2)
urls = list()
results = driver.find_elements_by_class_name("yuRUbf")

for re in results:
    href = re.find_element_by_tag_name("a").get_attribute("href")
    print(href)
    urls.append(href)


page = 2
while True:
    if page > pagination:
        break

    next_page = driver.find_element_by_link_text(str(page))
    next_page.click()
    time.sleep(2)
    results = driver.find_elements_by_class_name("yuRUbf")

    for re in results:
        href = re.find_element_by_tag_name("a").get_attribute("href")
        print(href)
        urls.append(href)
    page = page + 1
    time.sleep(5)

length = len(urls)
print(f"Got {length} link to get email from")

for url in urls:
    driver.get(url)
    time.sleep(2)
    href = driver.find_elements_by_tag_name("a")
    emails = list()
    mail = "No emails"
    for email in href:
        if email.get_attribute("href") and 'mailto' in email.get_attribute("href"):
            mail = email.get_attribute("href")[7:]
            print(emails)
            break
    print(mail)
    with open("datas.csv", "a", newline="") as f:
        writer = csv.writer(f)
        writer.writerow([url, mail)
        f.close()

