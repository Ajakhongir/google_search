from selenium import webdriver
import csv
import time

url = "https://www.google.co.za/search?q=stilbaai+property"

driver = webdriver.Chrome("chromedriver.exe")
driver.maximize_window()

driver.get(url)
time.sleep(2)
urls = list()
results = driver.find_elements_by_class_name("yuRUbf")

for re in results:
    href = re.find_element_by_tag_name("a").get_attribute("href")
    print(href)
    urls.append(href)


print(len(urls))

for url in urls:
    driver.get(url)
    time.sleep(2)
    href = driver.find_elements_by_tag_name("a")
    emails = list()
    mail = "No emails"
    for email in href:
        if email.get_attribute("href") and 'mailto' in email.get_attribute("href"):
            mail = email.get_attribute("href")
            print(emails)
            break
    print(mail)
    with open("datas.csv", "a", newline="") as f:
        writer = csv.writer(f)
        writer.writerow([url, mail[7:]])
        f.close()

